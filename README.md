# Fullbar Carrossel

# Instalação
Instale a biblioteca Vue2TouchEvents

```bash
npm install --save vue2-touch-events
```

Essa biblioteca é utilizada para o swipe do carrossel

# Configurando

No componente ou screen que você for usar o carrossel, importe ele e depois declare dentro de components

```javascript
import Carrossel from '@/components/Carrossel'

export default {
  components: {
    Carrossel
  }
}
```

# Como Usar

```javascript
<Carrossel />
```

Dentro do componente existe um array chamado data, onde estão os itens do carrossel, você pode modificar isso usando props para utilizar com o retorno da API